# -*- coding: utf-8 -*-
"""Check availablility of domain names using whois and the available ones"""
import random
import sys
import time

import hvplot
import hvplot.pandas  # noqa
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from whois import parser, whois

from domain_name_search.porkbun import df

#%%
max_cost = 200  # USD for 15 years
cheap = df[df.cost10_5 < max_cost].sort_values("cost5_10")
print(cheap, list(cheap.tld))

#%%

TLD = """.top .sbs .link .click .one .name .xyz .cc .rocks .observer .page
.art .club .quest .dev .science .info .stream .review .win .pro""".split()

preferred = cheap[cheap.tld.isin(TLD)]
print(preferred)

#%%

# Domain name prefixes of choice

NAMES = """
2pi
air
atmosphere
advect
algoflow
algofluid
alpha
ansatz
archives
ashwin
ashwinvis
binaryfluid
boundarylayer
circul
circulate
code
codeflow
compute
computed
computedreality
conserve
continuum
convect
convection
decode
digitalfluid
discover
discrete
dispersion
dynamics
dynamo
energycascade
enstrophy
fluid
fluidfunction
fluidlife
flow
graywizard
heatflux
hydro
iam
ideas
integral
integrate
inviscid
isabout
jetstream
journey
journeyto
kinetic
latlong
lifeof
liquid
liquidice
liquidlogic
lumino
luminous
mesh
mechanics
microscale
momentum
numerical
numeric
omega
open
paper
phasespace
phasespeed
physics
pi
potential
precipice
precision
research
researchof
resoft
river
scifluid
scitech
snapshot
spanning
startsat
stratified
streamfunc
streamfunction
streamfunk
tau
tensor
theta
town
ultraviolet
valley
virtualfluid
volume
wake
waterwind
watery
wave
wavemaker
wavenumber
wind
windwater
zeta
""".split()

#%%

# Init dataframe

avail = pd.DataFrame(preferred.tld)
#%%

# Query WHOIS


def check_avail(x, name):
    domain = f"{name}{x.tld}"
    try:
        # Use the system `whois` command to cover all TLDs
        result = whois(
            domain,
            command=True
            # , executable=("torsocks", "whois")
            # NOTE: While query using Tor would be good if blacklisted by a
            # WHOIS server, this requires some modification to the python-whois
            # package which right now only accepts single command.
        )
    except parser.PywhoisError as e:
        err = e.args[0]
        print(err, file=sys.stderr)
        domain_avail = "No match for" in err or "No Data Found" in err
    else:
        domain_avail = all(v is None for v in result.values())

    print(domain, "available:", domain_avail)
    time.sleep(3 + 2 * random.random())  # Simulated delays to avoid possible rate-limit
    return domain_avail


for name in NAMES:
    print("-" * 30)
    if name in avail.columns:
        print("ALREADY CHECKED. SKIPPING", name)
        continue

    print("CHECKING", name)
    avail[name] = avail.apply(lambda x: check_avail(x, name), 1)

    #%%


costs = pd.DataFrame(preferred.tld)
for name in avail.columns[1:]:
    costs[name] = (avail[name].astype("float") - 0.5) * 2 * preferred.cost10_5

costs.set_index(costs.tld, inplace=True)
del costs["tld"]

#%%

# Cache
df.to_parquet("./df.parquet")
cheap.to_parquet("./cheap.parquet")
preferred.to_parquet("./preferred.parquet")
costs.to_parquet("./costs.parquet")

#%%

# Reload data

df = pd.read_parquet("./df.parquet")
cheap = pd.read_parquet("./cheap.parquet")
preferred = pd.read_parquet("./preferred.parquet")
avail = pd.read_parquet("./avail.parquet")
costs = pd.read_parquet("./costs.parquet")


#%%

# Visualize costs for 10 years registration + 5 years renewal


fig, ax = plt.subplots(figsize=(30, 6), dpi=300)
sns.heatmap(
    costs,
    cmap="coolwarm",
    ax=ax,
    # annot=True, fmt=".1f", annot_kws={'fontsize': 'xx-small'}
)
#%%

fig = costs.hvplot.heatmap(
    x="columns",
    y="index",
    title="Estimated costs",
    cmap="coolwarm",
    xaxis="top",
    rot=70,
    width=1920,
    height=720,
)
# .opts(
#    fontsize={'title': 10, 'xticks': 5, 'yticks': 5}
# )
hvplot.show(fig)

#%%

shortlist = """
air.page
advect.stream
ashwin.observer
ashwinvis.page
boundarylayer.top
circulate.one
compute.club
digitalfluid.link
discover.one
fluid.link
fluid.observer
fluid.quest
fluid.science
graywizard.page
heatflux.top
journeyto.science
kinetic.click
lifeof.quest
liquidlogic.link
flow.review
wind.observer
virtualfluid.link
tau.top
streamfunk.one
stratified.art
stratified.science
startsat.top
spanning.xyz
scitech.link
scitech.rocks
research.page
precision.page
potential.page
"""

shortsort = shortlist.split()
shortsort.sort(key=lambda x: len(x))
