# domain-name-search

Search for cheap and available domain names on Porkbun

![](./data/plot_costs.png)

## Capabilities

- Scrap and estimate TLD costs
- Check availability of a domain name using whois
- Visualize costs in an interactive plot

## Limitations

- Premium domain names are not correctly identified
- Slow whois checks to avoid rate limiting

## How it works

1. Find the latest [https://archive.org snapshot of the Porkbun domain
   list](https://web.archive.org/web/https://porkbun.com/products/domains) (or
   by requesting a new snapshot by going to
   <https://web.archive.org/save/https://porkbun.com/products/domains>), and
   use that in `src/domain_name_search/porkbun.py`
1. Scrap the TLD prices to display them in a Pandas dataframe
1. Specify the TLD and NAMES you are interested in inside `data/check_avail.py`
1. Run whois queries and visualize them
