#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""TLD prices on Porkbun

Go to https://porkbun.com/products/domains and get an archived version of it.
Then execute this script (preferably in Spyder)

Created on Fri Aug 27 10:01:50 2021

@author: ashwinvis

Notes
-----

1. Obtain all tags which are structured as follows:

   .. raw::html

        <div class="domainsPricingAllExtensionsItem">
                 <div class="row">
            <div class="col-xs-3" ">
             <a href="/web/20210823031807/https://porkbun.com/tld/abogado">.abogado</a>
            </div>
            <div class="domainsPricingAllExtensionsItemPrice registration col-xs-3">
             $<span class="sortValue">31.89</span>
            </div>
            <div class="domainsPricingAllExtensionsItemPrice renewal col-xs-3">
             $<span class="sortValue">31.89</span>
            </div>
            <div class="domainsPricingAllExtensionsItemPrice transfer col-xs-3">
             $<span class="sortValue">27.95</span>
            </div>
                 </div>
        </div>

2. Convert it to a table (nested list) of text and then into a pandas DataFrame.
3. Estimate cost and display the cheapest options.

Glossary
--------
- TLD : Top level domain (.org, .net, .com ...)
- Regis : Short for registration
- Renew : Short for renewal
- Trans : Short for transfer

"""
import pandas as pd
import pooch
from bs4 import BeautifulSoup

#%%
html = pooch.retrieve(
    "https://web.archive.org/web/20210922125658/https://porkbun.com/products/domains",
    "83c51b456343b36a9a5f28ea0f6083d1853e48e98b9ada6581fad95e57ce10bb"
)
#%%
with open(html) as content:
    soup = BeautifulSoup(content, features="lxml")

"""
"""

tags = soup.find_all("div", class_="domainsPricingAllExtensionsItem")
#%%
table = [_.text.replace("1st Year Sale", "1stYearSale").split() for _ in tags]

# Deal with empty "Sale!" columns and add "NoSale" placeholder
table_filled = []
for row in table:
    row_filled = row[:1]
    idx = 1
    while idx < len(row):
        if idx == len(row) - 1 or row[idx + 1].startswith("$"):
            row_filled.extend([row[idx], "NoSale", row[idx]])
            step = 1
        else:
            row_filled.extend(row[idx : idx + 3])
            step = 3

        idx = idx + step

    table_filled.append(row_filled)

# print(table_filled[-8])
# print(table_filled[-7])
# print(table_filled[-6])

#%%
df = pd.DataFrame(
    table_filled,
    columns=(
        "tld",
        "regis_orig",
        "regis_sale",
        "regis_cur",
        "renew_orig",
        "renew_sale",
        "renew_cur",
        "trans_orig",
        "trans_sale",
        "trans_cur",
    ),
)
#%%


def clean_currency(x):
    """If the value is a string, then remove currency symbol and delimiters
    otherwise, the value is numeric and can be converted

    Courtesy: https://pbpython.com/currency-cleanup.html
    """
    if isinstance(x, str):
        return x.replace("$", "").replace(",", "")
    return x


# Remove $ and , symbols
for key in df.keys():
    if "orig" in key or "cur" in key:
        df[key] = df[key].apply(clean_currency).astype("float")
#%%


def estimate_cost(x, regis_years, renew_years):
    """Estimate cost for a number of years of registration and renewal
    for a TLD

    """
    if x.regis_sale in ("NoSale", "Sale!"):
        cost = regis_years * x.regis_cur
    elif x.regis_sale == "1stYearSale!":
        cost = x.regis_cur + (regis_years - 1) * x.regis_orig
    else:
        raise ValueError(repr(x))

    # We expect the original price for renewal after several years
    cost += renew_years * x.renew_orig
    return cost


df["cost5"] = df.apply(lambda x: estimate_cost(x, 5, 0), 1)
df["cost10"] = df.apply(lambda x: estimate_cost(x, 10, 0), 1)
df["cost5_10"] = df.apply(lambda x: estimate_cost(x, 5, 10), 1)
df["cost10_5"] = df.apply(lambda x: estimate_cost(x, 10, 5), 1)
